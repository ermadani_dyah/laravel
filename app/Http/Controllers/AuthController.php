<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }
    public function welcome(){
        return view('halaman_utama');
    }
    public function welcome_post(Request $request){
        //dd($request->all());
        $first=$request["first"];
        $last=$request["last"];
        return view('halaman_utama',["first"=>$first],["last"=>$last]);
    }
}
