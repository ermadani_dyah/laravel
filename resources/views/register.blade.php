<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Halaman Kedua</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
           <label for="First_nama">Fist Nama:</label> 
           <input type="text" name="first" id="first">
           <br><br>
           <label for="Last_name">Last Name:</label>
           <input type="text" name="last" id="last">
           <br><br>
           <label for="Gender">Gender:</label>
           <input type="radio" name="male" id="male">
           <label for="Male">Male</label>
           <input type="radio" name="female" id="female">
           <label for="Male">Female</label>
           <input type="radio" name="outher" id="outher">
           <label for="Male">Outher</label>
           <br><br>
           <label for="nationality">Nationality:</label>
           <select name="negara" id="negara">
               <option value="Indonesia">Indonesia</option>
               <option value="Jepang">Jepang</option>
               <option value="Korea">Korea Selatan</option>
           </select>
           <br><br>
           <label for="language">Language Spoken:</label>
           <input type="checkbox" name="indonesia" id="indonesia">
           <label for="C_Indonesia">Indonesia</label>
           <input type="checkbox" name="english" id="english">
           <label for="C_English">English</label>
           <input type="checkbox" name="outher" id="outher">
           <label for="C_Outher">Outher</label>
           <br><br>
           <label for="L_Bio">Bio:</label>
           <br>
           <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
           <br><br>
           <button type="submit">Sign Up</button>
        </form>
    </body>
</html>